from django.shortcuts import render, redirect
from .models import Status
from .forms import UserModelForm
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout

# Create your views here.

def index(request):
    stat = Status.objects.all(); 
    context = {}
    if request.method == 'POST':        
        form = UserModelForm(request.POST or None)
        if form.is_valid():
            form.save()
            form = UserModelForm()
            return HttpResponseRedirect('/')
    else:
        form = UserModelForm()

    return render(request, 'index.html',{'form': form,"stat":stat})

def story8(request):
    return render(request, 'story8.html')

@login_required(login_url='/loginpage')
def userpage(request):
    return render(request, 'userpage.html')

def logout_view(request):
    logout(request)
    return redirect('homepage:index')
