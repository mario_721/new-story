from django import forms
from .models import Status


class UserModelForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = '__all__'