from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('story8', views.story8, name='story8'),
    path('accounts/profile/', views.userpage, name='userpage'),
    path('loginpage', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logoutpage', views.logout_view, name='logout')
]
