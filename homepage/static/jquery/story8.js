async function doSearch(q) {
    const queryUrl = `https://www.googleapis.com/books/v1/volumes?q=${q}`;

    const data = await (await fetch(queryUrl)).json();

    const result = data.items;

    if(data) {
        console.log(result)
        $("#cont").html(result.map((item) => {
            const {title, authors, imageLinks, publisher, canonicalVolumeLink} = item.volumeInfo;
            return `
            <tr>
                <td>${title}</td>
                <td>${authors ? authors.join(", ") : "Unknown"}</td>
                <td>${publisher}</td>
                <td>${imageLinks ? `<img src=${imageLinks.thumbnail} />` : `No Image`}</td>
            </tr>
            `;
        }));
    }
    else {
        $("#cont").html("Tidak ditemukan!");
    }
}


$("document").ready(function() {
    doSearch("Harry Potter");
    $("#f").submit(async (e) => {
        e.preventDefault();

        $("#cont").html("Loading...");

        const input = $("#search").val();
        doSearch(input)
    })
});