from django.test import TestCase, Client, LiveServerTestCase
from .views import index
from .models import Status
from .forms import UserModelForm
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.auth import get_user_model
import chromedriver_binary

class unit_test(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_post_form_valid(self):
        form_data = {
            'status_field' : "aku senang sekali",
        }
        stat_form = UserModelForm(form_data)
        self.assertTrue(stat_form.is_valid())

    def test_header_true(self):
        response = Client().get('/')
        self.assertContains(response,"Halo Apa Kabar?")

class func_test(TestCase):
    def test_model_can_create_new_status(self):
        #Creating a new activity
        new_activity = Status.objects.create(date_field=timezone.now(),status_field='Aku Sedih')

        #Retrieving all available activity
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

class SeleniumTestCase(LiveServerTestCase):
    def setUp(self):
        # self.selenium = webdriver.Chrome("./chromedriver.exe")
        # super(SeleniumTestCase, self).setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        #super(SeleniumTestCase, self).tearDown()

    def test_function(self):
        selenium = self.selenium
        #Opening the link we want to test
        selenium.get(self.live_server_url)

        assert 'Halo Apa Kabar?' in selenium.page_source
        #find the form element
        status = selenium.find_element_by_name('status_field')
        submit = selenium.find_element_by_name('simpan')

        status.send_keys('Coba Coba')

        #submitting the form
        submit.send_keys(Keys.RETURN)

        #check the returned result
        assert 'Coba Coba' in selenium.page_source

class AuthTest(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')

    def test_login_redirect(self):
        response = self.client.get('/accounts/profile/', follow=True)
        self.assertContains(response, 'Please login')

    def test_secure_page(self):
        User = get_user_model()
        self.client.login(username='temporary', password='temporary')
        response = self.client.get('/accounts/profile/', follow=True)
        user = User.objects.get(username='temporary')
        self.assertContains(response, 'temporary')
